from app.models import Address
from typing import List

# def build_address_url(address_id: int) -> str:
#     return f"{request.host_url.strip('/')}{url_for('api_adresses.get', address_id=address_id,)}"


def serialize_address(address: Address) -> dict:
    return {
        'id': address.id,
        'street': address.street,
        'number': address.number,
        'addr_line_1': address.addr_line_1,
        'addr_line_2': address.addr_line_2,
        'postal_code': address.postal_code
    }


def serialize_address_list(address_list: List[Address]) -> List[dict]:
    return [serialize_address(address) for address in address_list]
